# Adavanced Physical Design using Open EDA tools
This repository consists the flow for an IP or SoC from RTL to GDS using Open EDA tools available. This is based on the  workshop conducted by Mr. Kunal Gosh, the SoC design planning in OpenLane flow in latest process node 130 nm by Google Skywater has been the described in detail.

# Overview of the workshop
Day 1 - Introduction and familiarisation to chip design flow and EDA Open tools 

Day 2 - Understanding the concepts of Floorplaning and Introduction to Library cells

Day 3 - Design and charcterisation of the Standard Cell with a design example of Inverter

Day4 -  Concepts of Pre-Layout timing analysis and Clock tree synthesis

Day 5- Complete flow from RTL2GDS

# Day 1 - Introduction and familiarisation to chip design flow and EDA Open tools

The IC design flow is a very structured process. Each step has a dedicated tools that performs the task, each design flow needs a good understanding of the design constraints. Now with Google Skywater PDK and availablity of open source tools, the potential of chip design can be explored even better.

The basic IC design flow involves
1. Block design (RTL)- Based on the design specifications, the blocks is first design and its functionality is verified by simulations and testbench.
2. Synthesis - This involves the realisation of the actual gates and logic blocks which performs the same functionality of the block design.
3.Layout - The netlist is converted to Physical connectivity. The flow begins with the Floorplaning, followed by Placement of the cells,performing timing delay analysis, synthesis of the clock tree and Routing. 

The tools used are :

1. Yosys – for Synthesis
2. Magic – for Layout and Floorplanning
3. CTS - TritonCTS - Synthesizes the clock distribution network
4. NgSpice - for Characterisation
5. OpenSTA – Pre-layout and Post-layout Static timing analysis

## OpenLane
This is an open source automated deign flow RTL2GDS. The figures shows the complete flow of RTS to GDS flow. 
![Openlane_flow](/uploads/2e52e79a89d44a3ad9f525889a9561f4/Openlane_flow.PNG)
## Initial steps for the Lab course
1. In the Linux terminal, open the Work directory. Check for the available folders -> openLane_flow and pdks
_pdks_ folder consists of all the foundry realted details like delays, PVT corners, device models.

2. Check the configuration file before starting the OpenLane, here the environment variables are set accordingly.

3. Invoke Openlane interactive flow

  ` ./flow.tcl -interactive`

4. Preparing the design -> The design here is picorv32a

  `prep -design picorv32a`

5. Store the results of the run directory in a customised folder. The command overwrite can be used if we want the results and settings to be overwriten

  `prep -design -tag last_run -overwrite`
6. Run the sythesis using following command
  `run_sythesis`

# Day 2 - Understanding the concepts of Floorplaning and Introduction to Library cells
   ## Floorplanning
   It is the crucial stage of the flow. It alloacted the place for the cells that need to be close, take consideration of the IO ports, core area utilisation. The pre defined blocks, Macros and Memory cells and their routing requirement can be determined during Floorplanning. This stage enables the effective area utilisation of the design.

   1. Run the Floorplan using following command
   `run_floorplan`
   2. Using Magic tool, the floorplan can be viewed. 
`magic -T <location of techfile> lef read <loction of lef file> def read <location of floorplan def file>`
   ## Placement
   1. Run placement using the following command
   
   `run_placement`

   
# Day 3 - Design and charcterisation of the Standard Cell with a design example of Inverter
Design of the Standard cell involves mainly 3 steps:
1. Inputs namely information on PDK, spice models for the design, DRC and LVS rules, user specified specifications for the design
2. The second stage involves the circuit level implementaion, layout design, testbench to verify the functionality of the design
3. The third stage is to obatin the output in standard forms like LEF, GDII

The Standard cell Inverted is used in the workshop. The design of it is cloned from the existing GIT
[https://github.com/nickson-jose/vsdstdcelldesign.git]

1. To clone from the Git respository

 ´git clone https://github.com/nickson-jose/vsdstdcelldesign.git`
 
 2. After cloning, goto to the vsdstdcelldesign directory and view the layout the Inverter _sky130_inv.mag_ file using Magic
 ![Inverter_layout](/uploads/4b4913367b6d3ea15c488d25d0b29ff8/Inverter_layout.PNG)
 3. The parasitic extraction can be done using the following commands

 `extract all`

 `ext2spice cthresh 0 rthresh 0`

 ` ext2spice`

 4. The extracted file is edited and transient simulation is set up


 5. The postlayout simulations of the extracted file is done by NGspice

`ngspice sky130_inv.spice`

![Inv_tran](/uploads/4ebd1de62e51d66498e03ebd421b50a5/Inv_tran.PNG)

The rise_time delay, fall_time delay,and other delays can be found out with the transient response of the Inverter

# Day4 -  Concepts of Pre-Layout timing analysis and Clock tree synthesis
1. Generating the standard cell LEF file from the Layout

Ensure the pins are in the intersection of the tracks, change the grid dimensions in tkon

`grid 0.46um 0.34um 0.23um 0.17um`
 
 To create LEF file, type in tkcon

 `lef write`

 2. Synthesis of the Standard cell

  Now our custom standard cell has to be in the OpenLane flow, and sythesis has to be done.
  Before synthesis, we need to have to include library which has the cell definations, for the abc to do the mapping during sythesis. This has the timing definations as well. Edit the config file, for the inverter.

    To include the custom LEF file to OpenLane, include the following commands:

    `set lefs [glob $::env(DESIGN_DIR)/src/*.lef]`
  
    `add_lefs -src $lefs`

 Now run the sythesis.

 3. Timeslack and correction of the timesclack

After the run synthesis, the timing slack will be displayed in the result. There are chances that a large slack exists in the sythesis stage itself. This has to be improvised. We can have to find the maximum fanout cells and increase the size of the buffers driving them. This way though area is increased, time slew can be reduced.

4. Run the sythesis again and check if the slack has reduced. Later run placement

5. Configure OpenSTA for post syntheis timing analysis:

create a configuration file pre_sta.conf. This consists of min, max library, verilog file and sdc 

![sta](/uploads/b9482c94a5ba6015b72410e4893a0b0f/sta.PNG)

6. Run_cts

7. Initiate Openroad in OpenLane as OpenSTA is already in the OpenLane
 `read_db pico_sv_cts.db`

`read_verilog <Absolute path where your verilog is>`

`read_liberty -max $::env(LIB_MAX)`

`read_sdc <sdc in your src folder>`
8. Use echo to change the maximum slack paths

# Day 5- Complete flow from RTL2GDS
1. Run PDN 
`gen_pdn`
2. To do the routing, type the command
` run_routing`

The DRC checks are performed and the GDII is as follows

![final_gdsii](/uploads/a43661c50bb8552cca1b2051cfd9e3a1/final_gdsii.PNG)


# Acknowledgements
1. Kunal Gosh
2. Nickson P Jose

# Referenced
[https://gitlab.com/gab13c/openlane-workshop]

[https://github.com/waleedbinehsan-lm/VSD-VLSI]

[https://github.com/nickson-jose/vsdstdcelldesign]

 






